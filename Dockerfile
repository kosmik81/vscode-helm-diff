FROM registry1-docker-io.repo.lab.pl.alcatel-lucent.com/node:8.12 as node
WORKDIR /work
COPY . ./
RUN cp .npmrc ~/
RUN npm install && npm run package
