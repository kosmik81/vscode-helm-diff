# vscode-helm-diff

This plugin provides instant feedback when working with Helm Chart value overrides.

## Features

  - Compares override values against chart's defaults and shows warnings for invalid overrides.
  - Command "Toggle helm diff" can be used to disable plugin
  - Output channel 'helm-diff' shows also comparison warnings and helm errors.

## Requirements

### Helm installed and initialized with access to your helm repositories

  - Get Helm from helm.sh and add it to $PATH.
  - Initialize Helm using 'helm init'. Use '--client-only' option if you don't wish to use Helm to deploy to k8s cluster.
  - Add some repositories with 'helm repo add [NAME] [URL]'

## Extension Settings

This extension contributes the following settings:

* `helm-diff.repos`: List of helm repos to look for charts, make sure you have added this repo to Helm. Default value: [ "neo-candidates" ]

## Usage
- If filename is 'values.yaml', 1st level keys are used as chart names to lookup. If 'app_list.yaml' is open side-by-side, it is used to lookup corresponding chart versions, otherwise comparison is against the latest versions.
- In case of other *.yaml files the first comment line is looked for *"# helm-diff: chartname"* annotation. In this case comparison is always against the latest versions.

## Known Issues

 - only 1st item of repo list is currently used to lookup charts
 - helm inspect values command fails often, even more often with --version parameter.

## Release Notes

### 1.0.1

Readme updates

### 1.0.0

Initial release

-----------------------------------------------------------------------------------------------------------
