'use strict';
// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
// import fs = require('fs')
// import path = require('path')
import yaml = require('js-yaml');
import diff = require('deep-diff');
import Promise = require('bluebird');
import path = require('path');
const exec = require('child_process').exec;

const editor = vscode.window.activeTextEditor;
// Output channel for messages
const outputChannel = vscode.window.createOutputChannel("helm-diff");

let enabled = true;
// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {

    // Use the console to output diagnostic information (console.log) and errors (console.error)
    // This line of code will only be executed once when your extension is activated
    console.log('Congratulations, your extension "vscode-helm-diff" is now active!');

    let repos = vscode.workspace.getConfiguration().get("helm-diff.repos");
    context.subscriptions.push(vscode.workspace.onDidChangeConfiguration( () => {
        repos = vscode.workspace.getConfiguration().get("helm-diff.repos");
    }));

    existsAsync("helm").then( exists => {
        if ( !exists ) {
            vscode.window.showErrorMessage('helm missing from the $PATH, get it from https://www.helm.sh/');
        }
    }).catch( err => {
        console.log(`err: ${err}`);
        vscode.window.showErrorMessage('helm missing from the $PATH, get it from https://www.helm.sh/');
    });

    context.subscriptions.push(outputChannel);

    // Command
    context.subscriptions.push(vscode.commands.registerCommand('extension.toggleHelmDiff', () => {
        // The code you place here will be executed every time your command is executed
        // Display a message box to the user
        if ( enabled ) {
            enabled = false;
            vscode.window.showInformationMessage("Helm diff disabled");
        } else {
            enabled = true;
            vscode.window.showInformationMessage("Helm diff enabled");
        }
    }));

    // Event handlers
    context.subscriptions.push(vscode.window.onDidChangeActiveTextEditor(activeEditor => {
        if ( activeEditor && enabled ) {
            updateRegExDiagnostics(activeEditor.document);
            helmValueWarnings(activeEditor.document, repos);
            // openDefaultValues(activeEditor.document);
        }
    }));
    context.subscriptions.push(vscode.workspace.onDidSaveTextDocument(doc => {
        if ( doc && enabled ) {
            helmValueWarnings(doc, repos);
        }
    }));
    context.subscriptions.push(vscode.window.onDidChangeTextEditorSelection(event => {
        if ( enabled ) {
            updateRegExDiagnostics(event.textEditor.document);
        }
    }));

    // Hover
    context.subscriptions.push(vscode.languages.registerHoverProvider('yaml', yamlParseHover ));

}

const yamlParseHover = {
    provideHover(doc: vscode.TextDocument) {
        if (editor) {
            let text = editor.document.getText();
            let native = parseYaml(text);
            let selectedText = editor.document.getText(editor.selection);
            return new vscode.Hover(JSON.stringify(native[selectedText]));
        } else {
            return;
        }
    }
};

const regExCollection = vscode.languages.createDiagnosticCollection("dCollection");

function updateRegExDiagnostics(document: vscode.TextDocument): void {
    if (document && document.languageId === 'yaml') {
        let diagnostics: vscode.Diagnostic[] = [];
        let severity = vscode.DiagnosticSeverity.Error;
        let message = "message";
        const regEx = /mikko/g;
        const text = document.getText();
        let match;
        let range;
        while((match = regEx.exec(text))) {
            console.log(`${Date.now()} match: ${match} at: ${match.index}`);
            const startPos = document.positionAt(match.index);
            const endPos = document.positionAt(match.index + match[0].length);
            range = new vscode.Range(startPos, endPos);
            if ( range ) {
                let diagnostic = new vscode.Diagnostic(range, message, severity);
                diagnostics.push(diagnostic);
            } else {
                console.log("warn: no range");
            }    
        }
        if (diagnostics) {
            regExCollection.set(document.uri, diagnostics);

        } else {
            regExCollection.clear();
        }
    } else {
        regExCollection.clear();
    }
}

function parseYaml(doc: string): any {
    let allValues: any;
    try {
        allValues = yaml.safeLoad(doc, { json: true } );
    } catch (e) {
        vscode.window.showErrorMessage(`Error while parsing yaml, error: ${e}`)
        return;
    }
    return allValues;
}

function helmValueWarnings(document: vscode.TextDocument, repos: any): void {
    if ( document.languageId !== "yaml" ) {
        return;
    }
    if ( path.basename(document.fileName) === 'values.yaml' ) {
        vscode.workspace.findFiles('**/app_list.yaml').then( uri => {
            console.log(`uri: ${uri}`);
        })
        let appList = [];
        let editors = vscode.window.visibleTextEditors;
        for (var i = 0; i < editors.length; i++) {
            console.log(`editors: ${JSON.stringify(editors[i].document.fileName)}`);
            if ( path.basename(editors[i].document.fileName) === 'app_list.yaml' ) {
                console.log("Found app_list.yaml")
                let text = editors[i].document.getText();
                appList = parseYaml(text)['HELM_CHARTS'];
                break;
            }
        }
        const text = document.getText();
        const allValues = parseYaml(text);
        let version = 0;
        for (let chartKey in allValues) {
            for (let  index = 0;  index < appList.length;  index++) {
                if ( appList[index]['name'] === chartKey) {
                    version = appList[index]['version'];
                };
            }
            outputChannel.appendLine(`key: ${chartKey}, version: ${version}`)
            reportWarningsAsync(repos, chartKey, allValues[chartKey], version);
        }
    } else if ( document.lineAt(0).text.includes("helm-diff:") ) {
            const chart = document.lineAt(0).text.split(':')[1]; // assume chart name in the 1st line
            const text = document.getText();
            const allValues = parseYaml(text);
            reportWarningsAsync(repos, chart.trim(), allValues);
    } else {
        return;
    }
}

function reportWarningsAsync(repos: Array<string>, chart: string, overrides: any, version?: any): void {
    getHelmValuesAsync(`${repos[0]}/${chart}`, version).then( values => {
        let defaults = parseYaml(values);
        console.log(`default values: ${JSON.stringify(defaults)}`);
        const failures = compareDocs(overrides, defaults);
        const failuresText: string[] = [];
        failures.forEach(failure => {
            let fText = failure.join('.');
            failuresText.push(fText);
        });
        const message = `Invalid overrides in ${chart}: ${JSON.stringify(failuresText.join('; '))}`;
        if (failures.length !== 0) {
            vscode.window.showWarningMessage(message);
            outputChannel.appendLine(message);
        }
    });
}

function compareDocs(overrides: any, defaults: any): any[] {
    var differences = diff.diff(overrides, defaults);
    console.log(`diff: ${JSON.stringify(differences)}`);
    const invalidOverrides: any[] = [];
    differences.forEach(difference => {
      switch (difference.kind) {
        case "N": // N - indicates a newly added property/element
          break;
        case "D": // D - indicates a property/element was deleted
          invalidOverrides.push(difference.path);
          break;
        case "E": // E - indicates a property/element was edited
          break;
        case "A": // A - indicates a change occurred within an array
          break;
        default:
      }
    });
    return invalidOverrides;
}

function getHelmValuesAsync(chart: string, version?: string): Promise<any> {
    let versionParam = ""
    if ( version ) {
        versionParam = `--version=${version}`
    }
    return runCmdAsync(`helm inspect values ${chart} ${versionParam}`).then( stdout => {
        return stdout;
    }).catch( err => {
        console.log(`ERROR : ${err}`);
        let msg = `Failed to run helm due to ${err}`
        vscode.window.showErrorMessage(msg);
        outputChannel.appendLine(msg);
    });
}

function runCmdAsync(cmd: string): Promise<string> {
    return new Promise((resolve, reject) => {
        exec(cmd, (error: any, stdout: any, stderr: any) => {
            if (error) { return reject(error); }
            if (stderr) { return reject(stderr); }
            resolve(stdout);
        });
    });
}

function existsAsync(cmd: string): Promise<boolean> {
    return runCmdAsync(`which ${cmd}`).then((stdout) => {
      if (stdout.trim().length === 0) {
        // maybe an empty command was supplied?
        // are we running on Windows??
        return Promise.reject(new Error('No output'));
      }
      const rNotFound = /^[\w\-]+ not found/g;
      if (rNotFound.test(cmd)) {
        return Promise.resolve(false);
      }
      return Promise.resolve(true);
    });
  }

/*
function openDocument(name: string, content: string): void {
    const rootPath = vscode.workspace.rootPath
    if ( rootPath ) {
        const filePath = path.join(rootPath, name);
        fs.writeFileSync(filePath, content, 'utf8');
        var openPath = vscode.Uri.parse(`${filePath}`);
        vscode.workspace.openTextDocument(openPath).then(doc => {
           vscode.window.showTextDocument(doc);
        });    
    } else {
        vscode.window.showErrorMessage("Can't open document, rootPath is not defined for unknown reason.")
    }
}

 function openDefaultValues(doc: vscode.TextDocument): void {
    const chart = doc.lineAt(0).text.substr(1).trim(); // assume chart name in the 1st line with single # comment
    getHelmValuesAsync(chart).then( yamlValues => {
        openDocument(`${chart}-default-values-${Date.now()}.yaml`, yamlValues);
    });
}
 */

// this method is called when your extension is deactivated
export function deactivate() {
}